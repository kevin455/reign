May be considered that I've coded all in windows 10

open cmd, cd to reign/front location and execute:
- docker build -t react_front .

cd to reign/server location and execute:
- docker build -t nestjs_server .

cd to reign root folder location and execute:
- docker-compose up


once database, server and front are up, go to http://localhost:4000/hacker-news/fillDatabase and then database
will be populated.
Open front in http://localhost:3000
