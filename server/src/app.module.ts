import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { HackerNewsModule } from './hacker-news/hacker-news.module';

@Module({
  imports: [
    HackerNewsModule,
    ScheduleModule.forRoot(),
    MongooseModule.forRoot(
      'mongodb://mongodb:27017/reign?authSource=admin&readPreference=primary&appname=MongoDB%20Compass&ssl=false',
    ),
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
