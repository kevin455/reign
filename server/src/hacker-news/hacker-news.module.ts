import { HttpModule, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsHitsSchema } from './hacker-news.model';
import { HackerNewsService } from './hacker-news.service';

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature([
      {
        name: 'HackerNews',
        schema: HackerNewsHitsSchema,
      },
    ]),
  ],
  controllers: [HackerNewsController],
  providers: [HackerNewsService],
})
export class HackerNewsModule {}
