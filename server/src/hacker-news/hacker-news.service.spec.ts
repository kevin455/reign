import { HttpModule } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Test, TestingModule } from '@nestjs/testing';
import { HackerNewsController } from './hacker-news.controller';
import { HackerNewsHitsSchema } from './hacker-news.model';
import { HackerNewsService } from './hacker-news.service';

describe('HackerNewsService', () => {
  let service: HackerNewsService;

  const mockHackerNews = {};

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        HttpModule,
        MongooseModule.forRoot(
          'mongodb://127.0.0.1:27017/reign?readPreference=primary&appname=MongoDB%20Compass&ssl=false',
        ),
        MongooseModule.forFeature([
          {
            name: 'HackerNews',
            schema: HackerNewsHitsSchema,
          },
        ]),
      ],
      controllers: [HackerNewsController],
      providers: [HackerNewsService],
    })
      .overrideProvider(HackerNewsService)
      .useValue(mockHackerNews)
      .compile();

    service = module.get<HackerNewsService>(HackerNewsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
