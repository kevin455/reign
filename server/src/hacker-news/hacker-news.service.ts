import { HttpService, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { Hits } from './types';
import { AxiosResponse } from 'axios';

@Injectable()
export class HackerNewsService {
  constructor(
    @InjectModel('HackerNews')
    private readonly hackerNewsModel: Model<Hits>,
    private httpService: HttpService,
  ) {}

  async findAndAddHackerNews(): Promise<AxiosResponse> {
    return await this.httpService
      .get('https://hn.algolia.com/api/v1/search_by_date?query=nodejs')
      .toPromise();
  }

  async create(data: Hits): Promise<void> {
    await this.hackerNewsModel.insertMany(data);
  }

  async getAllHackerNews(): Promise<Hits[]> {
    return this.hackerNewsModel.aggregate([
      {
        $sort: { created_at: -1 },
      },
    ]);
  }

  async deleteHackerNew(_id: string): Promise<Hits> {
    return this.hackerNewsModel.deleteOne({ _id }).lean();
  }
}
