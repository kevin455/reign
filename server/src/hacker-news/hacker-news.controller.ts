import { Controller, Delete, Get, Param } from '@nestjs/common';
import { HackerNewsService } from './hacker-news.service';

import { Cron } from '@nestjs/schedule';
import { Hits } from './types';

@Controller('hacker-news')
export class HackerNewsController {
  constructor(private readonly hackerNewsService: HackerNewsService) {}

  @Cron('0 * * * *')
  async findAndAddHackerNews() {
    const response = await this.hackerNewsService.findAndAddHackerNews();
    await this.hackerNewsService.create(response.data.hits);
  }

  @Get('/fillDatabase')
  async fillDatabase() {
    const response = await this.hackerNewsService.findAndAddHackerNews();
    await this.hackerNewsService.create(response.data.hits);
    return 'Done!';
  }

  @Get()
  async getAllHackerNews(): Promise<Hits[]> {
    const hackerNews: Hits[] = await this.hackerNewsService.getAllHackerNews();
    return hackerNews;
  }

  @Delete('/delete/:_id')
  async deleteHackerNew(@Param('_id') _id: string): Promise<Hits> {
    const response = await this.hackerNewsService.deleteHackerNew(_id);
    return response;
  }
}
