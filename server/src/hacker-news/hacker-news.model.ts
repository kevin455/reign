import * as mongoose from 'mongoose';

export const HackerNewsHitsHighlightResultSchema = new mongoose.Schema(
  {
    title: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      matchedWords: {
        type: [String],
      },
    },
    url: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      fullyHighlighted: {
        type: Boolean,
      },
      matchedWords: {
        type: [String],
      },
    },
    author: {
      value: {
        type: String,
      },
      matchLevel: {
        type: String,
      },
      matchedWords: {
        type: [String],
      },
    },
  },
  {
    timestamps: false,
  },
);

export const HackerNewsHitsSchema = new mongoose.Schema(
  {
    created_at: {
      type: Date,
    },
    title: {
      type: String,
    },
    url: {
      type: String,
    },
    author: {
      type: String,
    },
    points: {
      type: Number,
    },
    story_text: {
      type: String,
    },
    comment_text: {
      type: String,
    },
    num_comments: {
      type: Number,
    },
    story_id: {
      type: String,
    },
    story_title: {
      type: String,
    },
    story_url: {
      type: String,
    },
    parent_id: {
      type: String,
    },
    created_at_i: {
      type: Number,
    },
    _tags: {
      type: [String],
    },
    objectID: {
      type: String,
    },
    _highlightResult: HackerNewsHitsHighlightResultSchema,
  },
  {
    timestamps: false,
  },
);
