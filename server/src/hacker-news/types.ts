export interface _HighlightResult {
  title: {
    value: string;
    matchLevel: string;
    matchedWords: string[];
  };
  url: {
    value: string;
    matchLevel: string;
    fullyHighlighted: boolean;
    matchedWords: string[];
  };
  author: {
    value: string;
    matchLevel: string;
    matchedWords: string[];
  };
}

export interface Hits {
  created_at: Date;
  title: string;
  url: string;
  author: string;
  points: number;
  story_text: string;
  comment_text: string;
  num_comments: number;
  story_id: string;
  story_title: string;
  story_url: string;
  parent_id: string;
  created_at_i: number;
  _tags: string[];
  objectID: string;
  _highlightResult: _HighlightResult;
}
