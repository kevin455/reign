import React, { useState } from 'react'
import axios, { AxiosResponse } from 'axios'
import { useEffect } from 'react'
import styles from './index.module.css'
import HackerNews from './Components/HackerNews'

const { REACT_APP_SERVER } = process.env

const Home = (): JSX.Element => {

  const [data, setData] = useState([])
  const [reload, setReload] = useState(false)

  useEffect(() => {

    (async () => {
      const { data }: AxiosResponse<any> = await axios.get(`${REACT_APP_SERVER}/hacker-news`)
      setData(data)
    })()

  }, [reload])

  return (
    <>
      <div className={styles.container}>
        
        <div className={styles.header}>
          <span className={styles.title}>HN Feed</span>
          <span className={styles.subtitle}>We {'<3'} hacker news!</span>
        </div>
        <div className={styles.body}>
          
          <div className={styles.listContainer}>
            <HackerNews hackerNews={data} setReload={setReload} reload={reload}/>
          </div>

        </div>

      </div>
    </>
  )
}

export { Home }
