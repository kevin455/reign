import { Dispatch, SetStateAction } from "react"
import { Hits } from "../types"
import HackerNewsItem from "./HackerNewsItem"

interface HackerNewsProps {
  hackerNews: Hits[]
  setReload: Dispatch<SetStateAction<boolean>>
  reload: boolean
}

const HackerNews = ({ hackerNews, setReload, reload }: HackerNewsProps): JSX.Element => {

  return (
    <>
      {
        hackerNews.map((data: any, key: number) => {
          return <HackerNewsItem data={data} key={key} setReload={setReload} reload={reload} />
        })
      }
    </>
  )

}

export default HackerNews

