import React, { Dispatch, useState } from "react"
import axios, { AxiosResponse } from "axios"
import { FcEmptyTrash } from 'react-icons/fc'

import { formatDate } from "../../../Components/date/formatDate"

import styles from '../index.module.css'
import { Hits } from "../types"
import { SetStateAction } from "react"

const { REACT_APP_SERVER } = process.env

interface HackerNewsItemProps {
  data: Hits
  setReload: Dispatch<SetStateAction<boolean>>
  reload: boolean
}

const HackerNewsItem = ({ data, setReload, reload }: HackerNewsItemProps, key: number) => {

  const [showTrash, setShowTrash] = useState(false)

  const { _id, author, story_title, title, story_url, url, created_at } = data

  const hackerNewTitle = story_title === null ? title : title === null ? story_title : ''
      const hackerNewCreatedAt = formatDate(new Date(created_at))

  const handleShowTrash = (isVisible: boolean) => {
    setShowTrash(isVisible)
  }

  const openUrl = (story_url: string, url: string) => {
    const urlToOpen = story_url === null ? url : story_url
    window.open(urlToOpen, '_blank')
  }

  const deleteItem = async (_id: string) => {
    (async () => {
      const confirm = window.confirm('Do you want to delete this item?')
      
      if(confirm) {
        const response: AxiosResponse<any> = await axios.delete(`${REACT_APP_SERVER}/hacker-news/delete/${_id}`)
  
        if(response.data.deletedCount === 1) {
          setReload(!reload)
        }
      }
    })()
  }

  return (
    <div className={styles.item} key={key}
        onMouseOver={() => handleShowTrash(true)}
        onMouseLeave={() => handleShowTrash(false)}>
      <div className={styles.story_title} onClick={() => openUrl(story_url, url)}>
        {hackerNewTitle} <span className={styles.author}>- {author} -</span>
      </div>
      <div className={styles.story_createdAt} onClick={() => openUrl(story_url, url)}>
        {hackerNewCreatedAt}
      </div>
      <div className={styles.delete_story} onClick={() => deleteItem(_id)} >
        { showTrash && <FcEmptyTrash className={styles.delete_story_icon} /> }
      </div>
    </div>
  )
}

export default HackerNewsItem
