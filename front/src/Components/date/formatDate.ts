import { format, getDay, getMonth, isToday, isYesterday } from "date-fns";
import { month } from "./months";

const formatDate = (created_at: Date): string => {
  let hackerNewCreatedAt = "";

  if (isToday(created_at)) {
    hackerNewCreatedAt = format(created_at, "HH:mm a");
  } else if (isYesterday(created_at)) {
    hackerNewCreatedAt = "Yesterday";
  } else {
    const dateMonth = getMonth(created_at);
    const dateDay = getDay(created_at);
    const stringDate = `${month[dateMonth]} ${dateDay}`;

    hackerNewCreatedAt = stringDate;
  }

  return hackerNewCreatedAt;
};

export { formatDate };
