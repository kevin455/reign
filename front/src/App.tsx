import React from 'react';
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import { Root } from './Root';
import './app.module.css'

// Screens
import { Home } from './Screen/Home'

function App() {
  return (
    <BrowserRouter>
      <Route component={Root}>

        <Switch>
          <Route exact path="/" component={Home} />
        </Switch>

      </Route>
    </BrowserRouter>
  );
}

export default App;
