import { Children } from "../types.d"

const Root = ({ children }: Children) => {
  return children
}

export { Root }
